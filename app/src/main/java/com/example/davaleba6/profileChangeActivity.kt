package com.example.davaleba6

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RadioGroup
import com.example.davaleba6.databinding.ActivityMainBinding

class profileChangeActivity : AppCompatActivity() {

    private lateinit var changeButton : Button
    private lateinit var email : EditText
    private lateinit var name : EditText
    private lateinit var surname : EditText
    private lateinit var age : EditText
    private lateinit var btn_gender : RadioGroup
    private lateinit var gender : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_change)

        init()
    }
    private fun init(){
        changeButton = findViewById(R.id.profileChange)
        btn_gender = findViewById(R.id.btn_gender)
        email = findViewById(R.id.emailEditText)
        name = findViewById(R.id.nameEditText)
        surname = findViewById(R.id.surnameEditText)
        age = findViewById(R.id.ageEditText)

        btn_gender.setOnCheckedChangeListener { group, checkedId ->
            if (checkedId == R.id.radioButton){
                gender = "Female"
            }else if (checkedId == R.id.radioButton2){
                gender = "Male"
            }
        }

        changeButton.setOnClickListener {
            ChangedProfile()
        }
    }
    private fun ChangedProfile(){
        val i = intent

        i.putExtra("Name", name.text.toString())
        i.putExtra("Surname", surname.text.toString())
        i.putExtra("Email", email.text.toString())
        i.putExtra("Age", age.text.toString())
        i.putExtra("Gender", gender)

        setResult(Activity.RESULT_OK, i)

        finish()
    }
}