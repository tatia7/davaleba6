package com.example.davaleba6

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.example.davaleba6.databinding.ActivityMainBinding
import org.w3c.dom.Text

class profileActivity : AppCompatActivity() {

    companion object {
        private const val REQUEST_CODE = 11
    }

    private lateinit var changeProfile : Button
    private lateinit var name : TextView
    private lateinit var surname : TextView
    private lateinit var email : TextView
    private lateinit var age : TextView
    private lateinit var gender : TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.Theme_Davaleba6)
        setContentView(R.layout.activity_profile)



        init()
    }
    private fun init() {
        name = findViewById(R.id.nameTextView)
        surname = findViewById(R.id.surnameTextView)
        email = findViewById(R.id.emailTextView)
        age = findViewById(R.id.ageTextView)
        gender = findViewById(R.id.gender)
        changeProfile = findViewById(R.id.changeProfile)
        changeProfile.setOnClickListener{
            OpenActivity()
        }
    }
    private fun OpenActivity(){
        val intent = Intent(this, profileChangeActivity::class.java)
        startActivityForResult(intent, REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK){
            val Name = data?.extras?.getString("Name")
            val Surname = data?.extras?.getString("Surname")
            val Email = data?.extras?.getString("Email")
            val Age = data?.extras?.getString("Age")
            val Gender = data?.extras?.getString("Gender")

            name.setText(Name)
            surname.setText(Surname)
            email.setText(Email)
            age.setText(Age)
            gender.setText(Gender)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}